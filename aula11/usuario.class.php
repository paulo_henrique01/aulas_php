<?php

class Usuario
{
    private $id;
    public $nome;
    private $email;
    private $senha;

    public function __construct()
    {
        $this->objDb = new mysqli('localhost', 'root', '', 'aula_php', 3306);
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function setNome(string $nome)
    {
        $this->nome = $nome;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function setSenha(string $senha)
    {
        $this->senha = password_hash($senha, PASSWORD_DEFAULT);
    }

    public function getId(int $id)
    {
        return $this->id;
    }

    public function getNome(string $nome): string
    {
        return $this->nome;
    }

    public function getEmail(strin $email)
    {
        return $this->email;
    }

    public function getSenha(string $senha)
    {
        return $this->senha;
    }
    public function __destruct()
    {
        unset($this->objDb);
    }


    public function saveUsuario()
    {
        $objStmt = $this->objDb->prepare('REPLACE INTO tb_usuario(id, nome, email, senha)VALUES(?, ?, ?, ?)');
        $objStmt->bind_param('isss', $this->id, $this->nome, $this->email, $this->senha);
        if ($objStmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function apagarUsuario(int $id)
    {
        $objStmt = $this->objDb->prepare('DELETE from tb_usuario WHERE id = ?');
        $objStmt->bind_param('i', $this->id);
        if ($objStmt->execute()) {
            return true;
        } else {
            return false;
        }
    }
    public function buscarUsuario()
    {
        $objStmt = $this->objDb->prepare('SELECT nome,email,senha from tb_usuario');
    }
}
