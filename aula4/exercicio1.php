<?php
echo '<pre>';
//vetores bidimensionais 

$alunos = array(0 => array('Nome' => 'Luiz Fernando',
                           'bitbucket' => 'https://bit.'),
                1 => array('Nome' => 'Carlos',
                            'bitbucket' => 'https://bi.'),
                2 => array('Nome' => 'Douglas',
                           'bitbucket' => 'https://bit...'),
                3 => array('Nome' => 'Pedro',
                           'bitbucket' => 'https://bi...'),
                4 => array('Nome' => 'Paulo Henrique Novais',
                           'bitbucket' => 'https://bitbucket.org/paulo_henrique01')
            );       

//var_dump($alunos);
echo '<table border = 1>
       <thead>
       <th>Nome</th>
       <th>Bitbucket</th>
       </thead>';
for($i = 0; $i < count($alunos);$i++){
    echo "<tr>";
    echo "      <td>
                  {$alunos[$i]['Nome']}
                </td>
                <td>
                  {$alunos[$i]['bitbucket']}
               </td>
           </tr>";
}
echo'</table>';

echo'<br>';
echo'<br>';
echo'<br>';
//agora com foreach :-)
echo '<table border = 1>
       <thead>
       <th>Nome</th>
       <th>Bitbucket</th>
       </thead>';
foreach($alunos as $ind => $linha){
    echo"<tr>
          <td>
             {$linha['Nome']};
          </td>  
          <td>
             {$linha['bitbucket']};
          </td>
        </tr>";
}

echo'</table>';