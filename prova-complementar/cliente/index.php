<?php
session_start();

//Verifica se o usário pode acessar a funcionalidade
if (!isset($_SESSION['id_usuario'])) {

	header('Location: ../login.php');

	exit();
}

echo '<a href="../menu.php">Voltar</a><br><br>';
echo 'Aqui ser&atilde;o listados e cadastrados os Clientes<br><br>';

//Conecta no banco
if ($db = mysqli_connect('localhost', 'root', '', 'aula_php',3306))
{ } else {die("Problema ao conectar ao SGDB");}

//Funcionalidade APAGAR
if (isset($_POST['apagar'])) {
	if (is_numeric($_POST['apagar'])) {
		$p = mysqli_prepare($db, 'DELETE FROM cliente WHERE id = ?');
		mysqli_stmt_bind_param($p, 'i', $_POST['apagar']);
		mysqli_stmt_execute($p);
	}
}

$dados['nome'] = $dados['email'] = $dados['cpf'] = $id = null;

//Se for clicado o editar, recupera o cadastro do SGDB

if (isset($_POST['editar'])) {

	$id = preg_replace('/\D/', '', $_POST['editar']);

	$prep =	mysqli_prepare($db, 'SELECT nome, email,cpf	FROM cliente WHERE id = ?');
	mysqli_stmt_bind_param($prep, 'i', $id);

	mysqli_stmt_execute($prep);

	$result = mysqli_stmt_get_result($prep);

	$dados = $result->fetch_assoc();
}
//mostra form para o usuário
echo "	<form method='post'>
			<input 	type='hidden' name='id' value='$id'>
			Nome: <input type='text' name='nome' value='{$dados['nome']}'>
			<br>
			Email: <input type='text' name='email' value='{$dados['email']}'>
			<br>
			Cpf: <input type='text' name='cpf' value='{$dados['cpf']}'>
			<input  type='submit' value='salvar'>
		</form>";

//Se o usuário enviar algo, transforma em uma variável mais fácil de escrever		
$nome = isset($_POST['nome']) ? $_POST['nome'] : null;

$email = isset($_POST['email']) ? $_POST['email'] : null;

$cpf = isset($_POST['cpf']) ? $_POST['cpf'] : null;

if (empty($_POST['id'])) {
	//Faz consulta preparada para evitar SQL injection
	$preparada = mysqli_prepare($db, 'INSERT INTO cliente ( nome, email, cpf) VALUES ( ?, ?, ?)');

	mysqli_stmt_bind_param($preparada, 'sss', $nome, $email, $cpf);
	echo mysqli_stmt_error($preparada);

	if (mysqli_stmt_execute($preparada)) {

		echo "<br><br>Dados de $nome gravados no SGDB!";
	}
	//FIM Faz consulta preparada para evitar SQL injection

} elseif (is_numeric($_POST['id'])) {

	$p = mysqli_prepare($db, 'UPDATE cliente SET nome = ?,	email = ?, cpf = ? WHERE id = ?');

	mysqli_stmt_bind_param($p, 'sssi', $nome, $email, $cpf, $_POST['id']);

	if (mysqli_stmt_execute($p)) {
		echo "<br><br>Dados de $nome atualizados no SGDB!";
	}
}

//Como não tenho dados de usuário (perigoso), não preciso preprar a consulta SQL

$objConsulta = mysqli_query($db, 'SELECT id, nome, email, cpf FROM cliente');

echo "<form method='post'>";

echo "<table border=1>
		<tr>
			<td>ID</td>
			<td>NOME</td>
			<td>EMAIL</td>
			<td>CPF</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>";

while ($reg = $objConsulta->fetch_assoc()) {

	echo "	<tr>
				<td>{$reg['id']}</td>
				<td>{$reg['nome']}</td>
				<td>{$reg['email']}</td>
				<td>{$reg['cpf']}</td>
				<td><button name='editar'value='{$reg['id']}'>Editar</button></td>
				<td><button name='apagar' value='{$reg['id']}'>Apagar</button></td>				
			</tr>";
}

echo "</table>";
echo "</form>";
exit();

if (isset($_POST['nome'])) {
	$f = fopen($banco_de_dados, 'a');
	fwrite($f, "{$_POST['nome']};{$_POST['email']};{$_POST['cpf']}\n");
	fclose($f);
}

echo "<pre>";

if (is_file($banco_de_dados)) {
	$f = fopen($banco_de_dados, 'r');
	while ($linha = fgets($f)) {
		echo $linha;
	}
	fclose($f);
}
echo "</pre>";
