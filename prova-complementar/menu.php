<?php
if( session_status() !== PHP_SESSION_ACTIVE ){
	session_start();
}

//Verifica se o usário pode acessar a funcionalidade
if( !isset($_SESSION['id_usuario']) ){

	header('Location: ../login.php');

	exit();
}

echo "
<div>	
	<a href='cliente/'>Usu&aacute;rio</a> 
</div>
<div>
	<a href='aeronave/'>Aeronave</a>
</div>
<div>	
	<a href='sair.php'>Sair</a> 
</div>		
";